const { EOL } = require("os");

class App {
  constructor(input) {
    this.input = '';
    this.output = [];
    this.rowCount = 0;
    this.colCount = 0;
    this.Types = {
      NUMERIC: "NUMERIC",
      PIECE: "PIECE",
      END_ROW: "END_ROW",
      UNKNOWN: "UNKNOWN"
    };
  }

  // Parse the input
  parse(input) {
    const chars = input.split('');
    chars.forEach((char, i) => {
      if (this.isValidPiece(char)) {
        const pieceType = this.pieceType(char);
        const action = this.pieceAction(pieceType);
        this.output.push(action(char));
        if (this.colCount > 8) {
          throw new Error('Invalid column count');
        }
      } else {
        throw new Error(`Invalid character provided at position ${i}: ${char}`);
      }
    });
    if (this.rowCount !== 7) {
      throw new Error('Invalid row count');
    }
  }

  // Find out what the piece type is, we will use it later to generate an action
  pieceType(char) {
    const { NUMERIC, PIECE, END_ROW, UNKNOWN } = this.Types;
    if (!isNaN(+char)) {
      return NUMERIC;
    } else {
      if (char === "/") {
        return END_ROW;
      } else if (/[pkqbnr0-9]/i.test(char)) {
        return PIECE;
      } else {
        return UNKNOWN;
      }
    }
  }

  // Returns a function that does something with the character
  pieceAction(pieceType) {
    const { NUMERIC, PIECE, END_ROW, UNKNOWN } = this.Types;
    switch (pieceType) {
      // Numeric characters should returns a numer of .'s the number of which
      // is determined by the character itself.
      case NUMERIC:
        return (char) => {
          let string = '';
          for (let i = 0; i < char; i ++) {
            string += '.';
          }
          this.colCount += +char;
          return string;
        };
      // Pieces should simply return the character
      case PIECE:
        return (char) => {
          this.colCount += 1;
          return char
        };
      // End rows should print a line break
      case END_ROW:
        return () => {
          this.rowCount += 1;
          this.colCount = 0;
          return EOL
        };
      // Any unknowns should return an unknown character (perhaps a ?)
      case UNKNOWN:
      default:
        return () => {
          this.colCount += 1;
          return '?';
        }
    }
  }

  isValidPiece(char) {
    return true; // /[pkqbnr0-9/]/i.test(char);
  }

  logOutput() {
    console.log(this.output.join(''));
  }
}

module.exports = App;
