const { argv } = require("process");
const [program, file, input] = argv;

const App = require('./app.js');

const app = new App();
app.parse(input);
app.logOutput();
