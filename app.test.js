const test = require('ava');

const App = require('./app.js');

test('test for valid chars', t => {
  const app = new App();
  t.is(app.Types.NUMERIC, app.pieceType(0));
  t.is(app.Types.PIECE, app.pieceType('q'));
});
